module "deployment" {
    source      = "git::https://gitlab.com/meblogs/blog-terraform-at-scale-modualized-hierarchical-layout-any-continuous-delivery-of-infrastructure/templates/s3-lambda-parquet.git"
}

terraform {
  backend "s3" {}
}

provider "aws" {
  region = "us-east-1"
  profile = "dev"
}



